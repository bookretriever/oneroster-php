<?php

namespace OneRoster;

use InvalidArgumentException;

abstract class Link extends ApiObject
{
    public function __toString()
    {
        return $this->getHref();
    }

    // abstract public function fetch()

    public function getRelation()
    {
        return $this->get('relation');
    }

    public function getResourceType()
    {
        return $this->get('type');
    }

    public function getHref()
    {
        return $this->get('href');
    }

    protected function checkType($type)
    {
        if (!Types::has($type)) {
            throw new InvalidArgumentException('Invalid type' . (is_string($type) ? ': ' . $type : ''));
        }
    }
}
