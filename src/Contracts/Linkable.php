<?php

namespace OneRoster\Contracts;

interface Linkable
{
    /**
     * Retrieve the Link object with the given relation.
     *
     * @return \OneRoster\Link|\OneRoster\Link[]
     *
     * @throws \OneRoster\Exceptions\UnknownLinkException if the given link does not exist for this object.
     */
    public function getLink($linkRelation);

    /**
     * Retrieve all links.
     *
     * @return \OneRoster\Link[]
     */
    public function getLinks();
}
