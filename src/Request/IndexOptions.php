<?php

namespace OneRoster\Request;

use InvalidArgumentException;
use OneRoster\Filter;
use OneRoster\Internal\Arr;

class IndexOptions extends Options
{
    private $filter = null;
    private $limit = null;
    private $offset = null;
    private $orderBy = null;
    private $sort = null;

    public function getFilter()
    {
        return $this->filter;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function getOrderBy()
    {
        return $this->orderBy;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function loadQueryParameters($query)
    {
        parent::loadQueryParameters($query);

        if (Arr::has($query, 'limit')) {
            $value = Arr::get($query, 'limit');
            if (is_numeric($value)) {
                $value = intval($value);

                $this->setLimit($value);
            }
        }

        if (Arr::has($query, 'filter')) {
            $value = Arr::get($query, 'filter');
            if ($value) {
                $value = Filter::raw($value);

                $this->setFilter($value);
            }
        }

        if (Arr::has($query, 'offset')) {
            $value = Arr::get($query, 'offset');
            if (is_numeric($value)) {
                $value = intval($value);

                $this->setOffset($value);
            }
        }

        if (Arr::has($query, 'orderBy')) {
            $value = Arr::get($query, 'orderBy');

            if ($value) {
                $this->setOrderBy($value);
            }
        }

        if (Arr::has($query, 'sort')) {
            $value = Arr::get($query, 'sort');

            if ($value) {
                $this->setSort($value);
            }
        }
    }

    public function setFilter($filter)
    {
        if ($filter !== null) {
            if (!($filter instanceof Filter)) {
                throw new InvalidArgumentException('$filter must be instanceof' . Filter::class);
            }
        }

        $this->filter = $filter;
    }

    public function setLimit($limit)
    {
        if ($limit !== null) {
            if (!is_int($limit) || $limit < 0) {
                throw new InvalidArgumentException('$limit must be non-negative int or null');
            }
        }

        $this->limit = $limit;

        return $this;
    }

    public function setOffset($offset)
    {
        if ($offset !== null) {
            if (!is_int($offset) || $offset < 0) {
                throw new InvalidArgumentException('$offset must be non-negative int or null');
            }
        }

        $this->offset = $offset;

        return $this;
    }

    public function setOrderBy($orderBy)
    {
        if ($orderBy !== null) {
            if (!is_string($orderBy)) {
                throw new InvalidArgumentException('$orderBy must be a string');
            }

            $lowerOrderBy = strtolower($orderBy);

            if (!in_array($lowerOrderBy, ['asc', 'desc'], true)) {
                throw new InvalidArgumentException('Invalid $orderBy ' . $orderBy);
            }

            $orderBy = $lowerOrderBy;
        }

        $this->orderBy = $orderBy;
    }

    public function setSort($sort)
    {
        if ($sort !== null) {
            if (!is_string($sort)) {
                throw new InvalidArgumentException('$sort must be a string');
            }
        }

        $this->sort = $sort;
    }

    public function toQueryParameters()
    {
        $params = parent::toQueryParameters();

        $value = $this->getFilter();
        if ($value !== null && !$value->isEmpty()) {
            $params['filter'] = (string) $value;
        }

        $value = $this->getLimit();
        if ($value !== null) {
            $params['limit'] = $value;
        }

        $value = $this->getOffset();
        if ($value !== null) {
            $params['offset'] = $value;
        }

        $value = $this->getOrderBy();
        if ($value !== null) {
            $params['orderBy'] = $value;
        }

        $value = $this->getSort();
        if ($value !== null) {
            $params['sort'] = $value;
        }

        return $params;
    }
}
