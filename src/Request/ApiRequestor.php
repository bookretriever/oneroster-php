<?php

namespace OneRoster\Request;

/**
 * Objects that can be used to make API requests against OneRoster.
 */
interface ApiRequestor
{
    public function send($url, ApiOptions $options = null);
}
