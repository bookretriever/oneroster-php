<?php

namespace OneRoster\Request;

use OneRoster\Internal\Str;
use OneRoster\Internal\Util;
use JsonSerializable;
use Psr\Http\Message\MessageInterface as PsrResponse;

class Response implements JsonSerializable
{
    private $statusCode;
    private $sourceResponse;

    private $contentType;
    private $body = null;

    public function getContentType()
    {
        return $this->contentType;
    }

    public function getStatusClass()
    {
        return Util::intdiv($this->getStatusCode(), 100);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getHeader($header)
    {
        return $this->getSourceResponse()->getHeader($header);
    }

    public function getSourceResponse()
    {
        return $this->sourceResponse;
    }

    public function isJson()
    {
        if (!$this->contentType) {
            return false;
        }

        return Str::startsWith($this->contentType, 'application/json');
    }

    public function isSuccess()
    {
        return $this->getStatusClass() === 2;
    }

    public function jsonSerialize()
    {
        return [
            'status' => $this->getStatusCode(),
            'contentType' => $this->getContentType(),
            'body' => $this->getBody(),
        ];
    }

    private function setBody($body)
    {
        if ($this->isJson()) {
            $jsonBody = json_decode($body, false);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new UnexpectedValueException('error decoding json');
            }

            $this->body = $jsonBody;
        } else {
            $this->body = $body;
        }
    }

    private function setContentType($contentType)
    {
        $this->contentType = $contentType;

        $jsonType = 'application/json';
        if ($contentType && substr($contentType, 0, strlen($jsonType)) === $jsonType) {
            $this->bodyIsJson = true;
        } else {
            $this->bodyIsJson = false;
        }
    }

    public static function fromPsrResponse(PsrResponse $psrResponse)
    {
        $response = new self();
        $response->sourceResponse = $psrResponse;
        $response->statusCode = $psrResponse->getStatusCode();

        $contentType = $psrResponse->getHeader('Content-Type');
        if (count($contentType) > 0) {
            $contentType = $contentType[0];
        } else {
            $contentType = null;
        }

        $response->setContentType($contentType);

        $body = (string) $psrResponse->getBody();
        $response->setBody($body);

        return $response;
    }
}
