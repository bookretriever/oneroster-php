<?php

namespace OneRoster\Request;

use OneRoster\Exceptions\ApiRequestException;
use OneRoster\Exceptions\ResourceNotFoundException;
use OneRoster\Exceptions\RateLimitedException;
use OneRoster\Exceptions\TimeoutException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use GuzzleHttp\Psr7\Uri;

class GuzzleApiRequestor implements ApiRequestor
{
    private $clientId;
    private $clientSecret;
    private $bearerToken;

    public function setDefaultBearerToken($bearerToken)
    {
        $this->bearerToken = $bearerToken;
    }

    public function send($path, ApiOptions $options = null)
    {
        if (!$options) {
            $options = new ApiOptions();
        }

        $headers = $options->headers ?: [];
        $guzzleOptions = [];

        if ($options->bearerToken) {
            $headers['Authorization'] = 'Bearer ' . $options->bearerToken;
        } elseif ($options->clientId && $options->clientSecret) {
            $guzzleOptions['auth'] = [$options->clientId, $options->clientSecret];
        } elseif ($this->bearerToken) {
            $headers['Authorization'] = 'Bearer ' . $this->bearerToken;
        } elseif ($this->clientId && $this->clientSecret) {
            $guzzleOptions['auth'] = [$this->clientId, $this->clientSecret];
        }

        if ($options->timeout) {
            $guzzleOptions['timeout'] = $options->timeout;
        }

        if ($options->timeoutConnection) {
            $guzzleOptions['connect_timeout'] = $options->timeoutConnection;
        }

        if ($options->headers) {
            $headers = array_merge($headers, $options->headers);
        }

        if ($options->body) {
            $body = $options->body;
            if (!is_string($body)) {
                $body = json_encode($body);
                $headers['Content-Type'] = 'application/json';
            }
        } else {
            $body = null;
        }

        $client = $this->getClient();

        $path = (array) $path;
        array_unshift($path, $options->appId);

        $request = $this->buildRequest(
            $options->method ?: 'get',
            $options->baseUrl,
            $path,
            $options->queryParameters ?: [],
            $headers,
            $body
        );

        $guzzleResponse = $this->sendRequest($client, $request, $guzzleOptions);

        return $this->processResponse($request, $guzzleResponse);
    }

    private function buildRequest($method, $baseUrl, $path, array $queryParameters, array $headers, $body)
    {
        if (is_array($path)) {
            $path = implode('/', (array) $path);
        }

        $uri = new Uri($baseUrl . '/' . $path);

        if ($queryParameters) {
            $queryString = http_build_query($queryParameters);
            $uri = $uri->withQuery($queryString);
        }

        return new Request(strtoupper($method), $uri, $headers, $body);
    }

    private function formatRequestForException(Request $request)
    {
        return [
            'method' => $request->getMethod(),
            'uri' => (string) $request->getUri(),
        ];
    }

    private function getClient()
    {
        return new Client([
            'connect_timeout' => 5,
            'timeout' => 10,
        ]);
    }

    private function processResponse(Request $request, GuzzleResponse $response)
    {
        $response = Response::fromPsrResponse($response);

        $statusCode = $response->getStatusCode();
        $statusClass = $response->getStatusClass();

        if ($statusClass === 2) {
            return $response;
        } elseif ($statusClass === 4) {
            $formattedRequest = $this->formatRequestForException($request);
            switch ($statusCode) {
                case 404:
                    throw new ResourceNotFoundException($response, $formattedRequest);
                case 429:
                    throw new RateLimitedException($response, $formattedRequest);
                default:
                    throw new ApiRequestException($response, $formattedRequest);
            }
        }

        return new ApiRequestException($response, $this->formatRequestForException($request));
    }

    private function sendRequest(Client $client, Request $request, $requestOptions)
    {
        try {
            return $client->send($request, $requestOptions);
        } catch (ConnectException $e) {
            if (strpos($e->getMessage(), 'timed out') !== false) {
                throw new TimeoutException();
            }

            throw $e;
        } catch (ClientException $e) {
            $response = $e->getResponse();

            if (!$response) {
                throw $e;
            }

            return $response;
        }
    }
}
