<?php

namespace OneRoster;

use OneRoster\Exceptions\UnknownTypeException;
use OneRoster\Internal\Arr;
use OneRoster\Request\ApiRequestor;
use OneRoster\Request\RetrieveOptions;
use InvalidArgumentException;

class ResourceLink extends Link
{
    public function __construct($relation, $details, $multi = false)
    {
        $details = $this->parseDetails($relation, $details, $multi);

        parent::__construct($details);
    }

    public function getId()
    {
        return $this->getSourcedId();
    }

    public function getResource(RetrieveOptions $options = null, ApiRequestor $requestor = null)
    {
        $type = $this->getResourceType();
        if (!$type) {
            throw new UnknownTypeException('Link has no type');
        } elseif (!Types::has($type)) {
            throw new UnknownTypeException('Link type \'' . $type . '\' is unknown.');
        }

        $class = Types::getTypeClass($type);

        return call_user_func($class . '::retrieve', $this->getSourcedId(), $options, $requestor);
    }

    public function getSourcedId()
    {
        return $this->get('sourcedId');
    }

    public function isRelationMultiple()
    {
        return (bool) $this->get('relationMultiple');
    }

    private function parseDetails($relation, $details, $multi)
    {
        $requiredKeys = [
            'href',
            'sourcedId',
            'type',
        ];

        $parsedDetails = [];

        foreach ($requiredKeys as $key) {
            if (!Arr::has($details, $key)) {
                throw new InvalidArgumentException('$details.' . $key . ' must be set');
            }

            $value = Arr::get($details, $key);

            if (!is_string(Arr::get($details, $key))) {
                throw new InvalidArgumentException('$details.' . $key . ' must be a string');
            }

            $parsedDetails[$key] = $value;
        }

        if (Arr::has($parsedDetails, 'type')) {
            $this->checkType(Arr::get($parsedDetails, 'type'));
        }

        if (!$relation || !is_string($relation)) {
            throw new InvalidArgumentException('$relation must be a non-empty string');
        }

        $parsedDetails['relation'] = $relation;
        $parsedDetails['multi'] = (bool) $multi;

        return $parsedDetails;
    }
}
