<?php

namespace OneRoster;

class Enrollment extends ApiResource
{
    const ROLE_TEACHER = 'teacher';
    const ROLE_STUDENT = 'student';
    const ROLE_PARENT = 'parent';
    const ROLE_GUARDIAN = 'guardian';
    const ROLE_RELATIVE = 'relative';
    const ROLE_AIDE = 'aide';
    const ROLE_ADMINISTRATOR = 'administrator';

    public function getClass()
    {
        return $this->getLink('class');
    }

    public function getLinkRelations()
    {
        return [
            'class',
            'school',
            'user',
        ];
    }

    public function getPrimary()
    {
        return (bool) $this->primary;
    }

    public function getResourceType()
    {
        return 'enrollment';
    }

    public function getRole()
    {
        return $this->get('role');
    }

    public function getSchool()
    {
        return $this->getLink('school');
    }

    public function getUser()
    {
        return $this->getLink('user');
    }

    public static function getRoles()
    {
        return [
            static::ROLE_TEACHER,
            static::ROLE_STUDENT,
            static::ROLE_PARENT,
            static::ROLE_GUARDIAN,
            static::ROLE_RELATIVE,
            static::ROLE_AIDE,
            static::ROLE_ADMINISTRATOR,
        ];
    }
}
