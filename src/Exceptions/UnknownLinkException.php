<?php

namespace OneRoster\Exceptions;

class UnknownLinkException extends UnknownPropertyException
{
}
