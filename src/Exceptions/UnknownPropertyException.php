<?php

namespace OneRoster\Exceptions;

use UnexpectedValueException;

class UnknownPropertyException extends UnexpectedValueException
{
}
