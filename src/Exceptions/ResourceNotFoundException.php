<?php

namespace OneRoster\Exceptions;

class ResourceNotFoundException extends ApiRequestException
{
}
