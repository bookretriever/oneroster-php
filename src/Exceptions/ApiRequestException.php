<?php

namespace OneRoster\Exceptions;

use OneRoster\Request\Response;

class ApiRequestException extends Exception
{
    protected $request;
    protected $response;

    public function __construct(Response $response, $request, $message = null, $previous = null)
    {
        if (!$message) {
            $message = $response->getBody();
            if (!is_string($message)) {
                $message = json_encode($message);
            }
        }

        parent::__construct($message, $response->getStatusCode(), $previous);

        $this->response = $response;
        $this->request = $request;
    }

    final public function getRequest()
    {
        return $this->request;
    }

    final public function getResponse()
    {
        return $this->response;
    }
}
