<?php

namespace OneRoster\Exceptions;

use UnexpectedValueException;

class UnknownGradeException extends UnexpectedValueException
{
}
