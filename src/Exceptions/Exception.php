<?php

namespace OneRoster\Exceptions;

use RuntimeException;

class Exception extends RuntimeException
{
}
