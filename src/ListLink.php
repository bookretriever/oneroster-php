<?php

namespace OneRoster;

use OneRoster\Exceptions\UnknownTypeException;
use OneRoster\Internal\Header;
use OneRoster\Internal\Uri;
use OneRoster\Request\ApiRequestor;
use OneRoster\Request\IndexOptions;
use InvalidArgumentException;

class ListLink extends Link
{
    public function __construct($type, $relation, $href)
    {
        $details = $this->parseDetails($type, $relation, $href);

        parent::__construct($details);
    }

    public function getResources(IndexOptions $options = null, ApiRequestor $requestor = null)
    {
        $type = $this->getResourceType();
        if (!$type) {
            throw new UnknownTypeException('Link has no type');
        } elseif (!Types::has($type)) {
            throw new UnknownTypeException('Link type \'' . $type . '\' is unknown.');
        }

        $class = Types::getTypeClass($type);

        $uri = $this->getHref();
        $query = Uri::getQueryString($uri);
        if ($options) {
            $options->loadQueryString($query);
        } else {
            $options = IndexOptions::fromQueryString($query);
        }

        return call_user_func($class . '::index', $options, $requestor);
    }

    public static function fromLinkHeader($type, $headerValue)
    {
        if (!$headerValue) {
            return [];
        }

        $links = [];

        $parsed = Header::parseLink($headerValue);
        foreach ($parsed as $parsedLink) {
            $relation = $parsedLink['rel'];
            if (!$relation) {
                throw new InvalidArgumentException('Link missing rel');
            }

            $relation = $relation[0];

            $uri = $parsedLink['_uri'];
            $link = new self($type, $relation, $uri);

            $links[$relation] = $link;
        }

        return $links;
    }

    private function parseDetails($type, $relation, $href)
    {
        $this->checkType($type);

        if (!$relation || !is_string($relation)) {
            throw new InvalidArgumentException('$relation must be a non-empty string');
        }

        if (!$href || !is_string($href)) {
            throw new InvalidArgumentException('$href must be a non-empty string');
        }

        $parsedDetails['type'] = $type;
        $parsedDetails['relation'] = $relation;
        $parsedDetails['href'] = $href;

        return $parsedDetails;
    }
}
