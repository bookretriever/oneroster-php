<?php

namespace OneRoster;

use OneRoster\Internal\Date;

class AcademicSession extends ApiResource
{
    const TYPE_TERM = 'term';
    const TYPE_GRADING_PERIOD = 'gradingPeriod';
    const TYPE_SCHOOL_YEAR = 'schoolYear';
    const TYPE_SEMESTER = 'semester';

    public function getChildren()
    {
        return $this->getLinkTry('children');
    }

    public function getEndDate()
    {
        return Date::parseDate($this->get('endDate'));
    }

    public function getLinkRelations()
    {
        return [
            'children',
            'parent',
        ];
    }

    public function getParent()
    {
        return $this->getLinkTry('parent');
    }

    public function getResourceType()
    {
        return 'academicSession';
    }

    public function getStartDate()
    {
        return Date::parseDate($this->get('startDate'));
    }

    public function getTitle()
    {
        return $this->get('title');
    }

    public function getType()
    {
        return $this->get('type');
    }

    public static function getTypes()
    {
        return [
            static::TYPE_TERM,
            static::TYPE_GRADING_PERIOD,
            static::TYPE_SCHOOL_YEAR,
            static::TYPE_SEMESTER,
        ];
    }
}
