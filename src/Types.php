<?php

namespace OneRoster;

use OneRoster\Request\ApiRequestor;
use OneRoster\Request\RetrieveOptions;
use InvalidArgumentException;

final class Types
{
    private static $registered = false;
    private static $registrations = [];

    public static function all()
    {
        static::register();

        return static::$registrations;
    }

    public static function getTypeClass($type)
    {
        if (static::has($type)) {
            return static::$registrations[$type];
        }

        return null;
    }

    public static function has($type)
    {
        static::register();

        return isset(static::$registrations[$type]);
    }

    public static function hasType($type)
    {
        return static::has($type);
    }

    public static function retrieve($type, $id, RetrieveOptions $options = null, ApiRequestor $requestor = null)
    {
        $class = static::getTypeClass($type);
        if (!$class) {
            throw new InvalidArgumentException('Unknown type ' . $type);
        }

        $empty = new $class();
        $resource = $empty->retrieve($id, $options, $requestor);

        return $resource;
    }

    private static function register()
    {
        if (static::$registered) {
            return;
        }

        $types = [
            'academicSession' => AcademicSession::class,
            'class' => CourseClass::class,
            'course' => Course::class,
            'demographics' => Demographic::class,
            'enrollment' => Enrollment::class,
            'org' => Org::class,
            'user' => User::class,
        ];

        static::$registrations = $types;
        static::$registered = true;
    }
}
