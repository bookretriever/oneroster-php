<?php

namespace OneRoster;

class Org extends ApiResource
{
    const TYPE_NATIONAL = 'national';
    const TYPE_STATE = 'state';
    const TYPE_LOCAL = 'local';
    const TYPE_SCHOOL = 'school';

    public function getChildren()
    {
        return $this->getLinkTry('children');
    }

    public function getIdentifier()
    {
        return $this->get('identifier');
    }

    public function getLinkRelations()
    {
        return [
            'children',
            'parent',
        ];
    }

    public function getName()
    {
        return $this->get('name');
    }

    public function getParent()
    {
        return $this->getLinkTry('parent');
    }

    public function getResourceType()
    {
        return 'org';
    }

    public function getType()
    {
        return $this->get('type');
    }

    public static function getTypes()
    {
        return [
            static::TYPE_SCHOOL,
            static::TYPE_LOCAL,
            static::TYPE_STATE,
            static::TYPE_NATIONAL,
        ];
    }
}
