<?php

namespace OneRoster\Internal;

use InvalidArgumentException;
use stdClass;

class Obj
{
    private function __construct()
    {
        // Static methods only
    }

    public static function get($obj, $key, $default = null)
    {
        if (is_string($key)) {
            $keyParts = explode('.', $key);
        } elseif (is_array($key)) {
            $keyParts = $key;
        } elseif (is_int($key)) {
            $keyParts = [(string) $key];
        } else {
            throw new InvalidArgumentException('$key invalid type ' . gettype($key));
        }

        $currentItem = $obj;
        for ($i = 0; $i < count($keyParts) - 1; ++$i) {
            $part = $keyParts[$i];

            if (!static::issetSubkey($currentItem, $part)) {
                return $default;
            }

            $currentItem = static::getSubkey($currentItem, $part);
        }

        $lastPart = $keyParts[count($keyParts) - 1];

        if (static::issetSubkey($currentItem, $lastPart)) {
            return static::getSubkey($currentItem, $lastPart);
        }

        return $default;
    }

    public static function getType($obj)
    {
        $type = gettype($obj);
        if ($type === 'object') {
            return get_class($obj);
        }

        return $type;
    }

    public static function set(&$obj, $key, $value)
    {
        if (is_string($key)) {
            $keyParts = explode('.', $key);
        } elseif (is_array($key)) {
            $keyParts = $key;
        } elseif (is_int($key)) {
            $keyParts = [(string) $key];
        } else {
            throw new InvalidArgumentException('$key invalid type ' . gettype($key));
        }

        $currentItem = &$obj;
        for ($i = 0; $i < count($keyParts) - 1; ++$i) {
            $part = $keyParts[$i];

            if (!static::issetSubkey($currentItem, $part)) {
                static::setSubkey($currentItem, $part, static::newSubkey($currentItem));
            }

            $currentItem = &static::getSubkey($currentItem, $part);
        }

        $lastPart = $keyParts[count($keyParts) - 1];
        static::setSubkey($currentItem, $lastPart, $value);
    }

    private static function &getSubkey(&$obj, $key)
    {
        if (is_array($obj)) {
            return $obj[$key];
        }

        return $obj->{$key};
    }

    private static function issetSubkey($obj, $key)
    {
        if (is_array($obj)) {
            return isset($obj[$key]);
        }

        return isset($obj->{$key});
    }

    private static function newSubkey($obj)
    {
        if (is_array($obj)) {
            return [];
        }

        return new stdClass();
    }

    private static function setSubkey(&$obj, $key, $value)
    {
        if (is_array($obj)) {
            $obj[$key] = $value;
        } else {
            $obj->{$key} = $value;
        }
    }
}
