<?php

namespace OneRoster\Internal;

use stdClass;

class Arr
{
    private function __construct()
    {
    }

    public static function get($collection, $key)
    {
        if (is_array($collection)) {
            return $collection[$key];
        }

        return $collection->{$key};
    }

    public static function has($collection, $key)
    {
        if (is_array($collection)) {
            return isset($collection[$key]);
        }

        return isset($collection->{$key});
    }

    public static function isAllInstanceOf($arr, $class)
    {
        if (!static::like($arr)) {
            throw new InvalidArgumentException('Not array-like');
        }

        foreach ($arr as $value) {
            if (!is_a($value, $class)) {
                return false;
            }
        }

        return true;
    }

    public static function isPurelyAssociative($arr)
    {
        if (!static::like($arr)) {
            return false;
        }

        foreach ($arr as $key => $ignored) {
            if (!is_string($key)) {
                return false;
            }

            if (is_numeric($key)) {
                return false;
            }
        }

        return true;
    }

    public static function isPurelyIndexed($arr)
    {
        if (!is_array($arr)) {
            return false;
        }

        foreach ($arr as $key => $ignored) {
            if (is_string($key)) {
                return false;
            }
        }

        return true;
    }

    public static function last($collection)
    {
        end($collection);

        return current($collection);
    }

    public static function like($collection)
    {
        return is_array($collection) || $collection instanceof stdClass;
    }

    public static function set(&$collection, $key, $value)
    {
        if (is_array($collection)) {
            $collection[$key] = $value;
        } else {
            $collection->{$key} = $value;
        }
    }
}
