<?php

namespace OneRoster\Internal;

class Util
{
    private function __construct()
    {
    }

    public static function intdiv($dividend, $divisor)
    {
        // @codingStandardsIgnoreStart
        return intdiv($dividend, $divisor);
        // @codingStandardsIgnoreEnd
    }

    /**
     * Converts a grade string to a number.
     * Kindergarten is converted to 0, pre-k to -1, and post-grade to 17.
     * 'Other' is returned as null.
     * Post-grad is 17 to leave room for college years.
     *
     * @param string $grade The grade to convert.
     *
     * @return int The converted grade value.
     */
    public static function gradeToNumeric($grade)
    {
        if (!$grade) {
            return null;
        }

        if (!is_string($grade)) {
            throw new UnknownGradeException('Expected grade to be a string');
        }

        if (is_numeric($grade)) {
            $grade = intval($grade);

            if (1 <= $grade && $grade <= 12) {
                return $grade;
            }
        }

        $lowerGrade = strtolower($grade);

        if ($lowerGrade === 'other') {
            return null;
        }

        $gradeMapping = [
            'prekindergarten' => -1,
            'kindergarten' => 0,
            'postgraduate' => 17,
        ];

        if (isset($gradeMapping[$lowerGrade])) {
            return $gradeMapping[$lowerGrade];
        }

        throw new UnknownGradeException('Unknown student grade \'' . $grade . '\'');
    }
}
