<?php

namespace OneRoster\Internal;

use HTTP2;

class Header
{
    private function __construct()
    {
        // Static methods only
    }

    public static function parseLink($header)
    {
        $http = new HTTP2();
        $links = $http->parseLinks($header);

        return $links;
    }
}
