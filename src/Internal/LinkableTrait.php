<?php

namespace OneRoster\Internal;

use OneRoster\Exceptions\UnknownLinkException;
use OneRoster\Link;
use OneRoster\ApiObject;
use OneRoster\ResourceLink;

trait LinkableTrait
{
    /**
     * @see \OneRoster\Contracts\Linkable::getLink()
     */
    public function getLink($relation)
    {
        if (!is_string($relation)) {
            throw new InvalidArgumentException('$relation not string');
        }

        $relations = $this->getLinkRelations();
        if (!in_array($relation, $relations, true)) {
            throw new UnknownLinkException('Link relation \'' . $relation . '\' not defined on type');
        }

        $link = $this->{$relation};
        if ($link === null) {
            throw new UnknownLinkException('Link relation \'' . $relation . '\' not in resource instance');
        }

        if ($link instanceof Link) {
            return $link;
        }

        if ($link instanceof ApiObject) {
            $link = $link->toArray();
        }

        if (Arr::like($link) && Arr::isAllInstanceOf($link, Link::class)) {
            return $link;
        }

        throw new UnknownLinkException('Link relation \'' . $relation . '\' not instanceof ' . Link::class);
    }

    /**
     * @see \OneRoster\Contracts\Linkable::getLinks()
     */
    public function getLinks()
    {
        $links = [];

        $relations = $this->getLinkRelations();
        foreach ($relations as $relation) {
            $link = $this->{$relation};
            if (!$link) {
                return;
            }

            $links[] = $link;
        }

        return $links;
    }

    public function getLinkRelations()
    {
        return [];
    }

    protected function transformData($key, $value)
    {
        $relations = $this->getLinkRelations();
        if (!in_array($key, $relations, true)) {
            return $value;
        }

        return $this->transformToLink($key, $value);
    }

    protected function transformToLink($relation, $value)
    {
        $link = null;
        if (Arr::isPurelyIndexed($value)) {
            $link = [];
            foreach ($value as $sublink) {
                $link[] = new ResourceLink($relation, $sublink, true);
            }
        } else {
            $link = new ResourceLink($relation, $value);
        }

        return $link;
    }
}
