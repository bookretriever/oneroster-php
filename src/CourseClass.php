<?php

namespace OneRoster;

class CourseClass extends ApiResource
{
    const TYPE_HOMEROOM = 'homeroom';
    const TYPE_SCHEDULED = 'scheduled';

    public function getClassCode()
    {
        return $this->classCode;
    }

    public function getClassType()
    {
        return $this->classType;
    }

    public function getCourse()
    {
        return $this->getLinkTry('course');
    }

    public function getGrade()
    {
        return $this->grade;
    }

    public function getIndexType()
    {
        return 'classes';
    }

    public function getLinkRelations()
    {
        return [
            'course',
            'school',
            'terms',
        ];
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getResourceType()
    {
        return 'class';
    }

    public function getSchool()
    {
        return $this->getLink('school');
    }

    public function getSubjects()
    {
        return $this->subjects ?: [];
    }

    public function getTerms()
    {
        return $this->getLinkTry('terms') ?: [];
    }

    public function getTitle()
    {
        return $this->get('title');
    }

    public static function getClassTypes()
    {
        return [
            static::TYPE_HOMEROOM,
            static::TYPE_SCHEDULED,
        ];
    }
}
