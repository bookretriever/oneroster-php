<?php

namespace OneRoster;

use BadMethodCallException;
use OneRoster\Contracts\Linkable;
use OneRoster\Exceptions\UnexpectedResponseException;
use OneRoster\Exceptions\UnknownLinkException;
use OneRoster\Internal\Date;
use OneRoster\Internal\LinkableTrait;
use OneRoster\Request\ApiOptions;
use OneRoster\Request\ApiRequestor;
use OneRoster\Request\DefaultApiRequestor;
use OneRoster\Request\IndexOptions;
use OneRoster\Request\Options;
use OneRoster\Request\RetrieveOptions;

/**
 * An ApiObject that has an API endpoint.
 */
abstract class ApiResource extends ApiObject implements Linkable
{
    use LinkableTrait;

    public function getBasePath()
    {
        return implode('/', [
            'learningdata',
            'v1',
            $this->getIndexType(),
        ]);
    }

    public function getId()
    {
        return $this->getSourcedId();
    }

    public function getDateLastModified()
    {
        $dateLastModified = $this->dateLastModified;
        if (!$dateLastModified) {
            // The spec say dateLastModified MUST be present, but some sample data from ClassLink has this missing.
            return null;
        }

        $dateLastModified = Date::parseDateTime($dateLastModified);

        return $dateLastModified;
    }

    /**
     * Get the path for listing all resources.
     *
     * @return string[] The index path.
     */
    public function getIndexPath()
    {
        return [
            $this->getBasePath(),
        ];
    }

    public function getIndexType()
    {
        return $this->getResourceType() . 's';
    }

    /**
     * Get the path for retrieving the resource with the given resource ID.
     *
     * @param string $id The resource ID.
     *
     * @return string[] The retrieval path.
     */
    public function getRetrievePath($id)
    {
        return [
            $this->getBasePath(),
            $id,
        ];
    }

    abstract public function getResourceType();

    public function getSourcedId()
    {
        return $this->get('sourcedId');
    }

    public function getStatus()
    {
        return $this->get('status');
    }

    protected function getLinkTry($linkName)
    {
        try {
            return $this->getLink($linkName);
        } catch (UnknownLinkException $e) {
            return null;
        }
    }

    public static function index(IndexOptions $options = null, ApiRequestor $requestor = null)
    {
        $empty = static::makeInternal(__METHOD__);
        $path = $empty->getIndexPath();

        $response = static::sendRequest($path, $options, $requestor);
        if (!$response->isJson()) {
            throw new UnexpectedResponseException($response, 'Expected response body to be json');
        }

        $body = $response->getBody();

        if (!isset($body->data)) {
            $body->data = [];
        }

        $type = $empty->getResourceType();
        $indexType = $empty->getIndexType();

        $countHeader = $response->getHeader('X-Total-Count');
        if ($countHeader) {
            $count = $countHeader[0];
            if (is_numeric($count)) {
                $count = intval($count);
            } else {
                $count = null;
            }
        } else {
            $count = null;
        }

        $linkHeader = $response->getHeader('Link');
        if ($linkHeader) {
            $links = ListLink::fromLinkHeader($type, $linkHeader);
        } else {
            $links = [];
        }

        $data = $body->{$indexType};

        $items = [];
        foreach ($data as $responseItem) {
            $item = new static($responseItem);

            // $item->data['original'] = $response->getSourceResponse();
            $items[] = $item;
        }

        $resourceList = new ApiResourceList($type, $items, $links, $count);

        return $resourceList;
    }

    public static function retrieve($id, RetrieveOptions $options = null, ApiRequestor $requestor = null)
    {
        $empty = static::makeInternal(__METHOD__);
        $path = $empty->getRetrievePath($id);

        $response = static::sendRequest($path, $options, $requestor);
        if (!$response->isJson()) {
            throw new UnexpectedResponseException($response, 'Expected response body to be json');
        }

        $body = $response->getBody();
        $type = $empty->getResourceType();

        $data = $body->{$type};

        $resource = new static($data);

        return $resource;
    }

    public static function make($methodName = null)
    {
        return static::makeInternal(__METHOD__);
    }

    private static function makeInternal($methodName)
    {
        $class = static::class;
        if ($class === self::class) {
            throw new BadMethodCallException($methodName . ' must be called from a base class');
        }

        return new $class();
    }

    private static function sendRequest($path, Options $options = null, ApiRequestor $requestor = null)
    {
        $requestor = DefaultApiRequestor::get($requestor);
        $apiOptions = new ApiOptions();
        if (!$options) {
            $options = new Options();
        }

        $apiOptions->queryParameters = $options->toQueryParameters();

        if ($options->getAppId()) {
            $apiOptions->appId = $options->getAppId();
        }

        if ($options->getBearerToken()) {
            $apiOptions->bearerToken = $options->getBearerToken();
        }

        $bearerToken = $apiOptions->bearerToken;
        $response = $requestor->send($path, $apiOptions);

        return $response;
    }
}

function dump($value)
{
    // TODO: Remove
    echo json_encode($value, JSON_PRETTY_PRINT) . "\n";
}
