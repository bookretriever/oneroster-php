<?php

namespace OneRoster;

use OneRoster\Contracts\Linkable;
use OneRoster\Exceptions\UnknownLinkException;
use OneRoster\Internal\AbstractList;
use InvalidArgumentException;

class ApiResourceList extends AbstractList implements Linkable
{
    protected $links;
    protected $count;
    protected $type;

    public function __construct($type, $items, $links, $count = null)
    {
        $this->setType($type);

        parent::__construct($this->processItems($items));

        $this->setLinks($links);
        $this->setCount($count);
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getLink($relation)
    {
        if (!isset($this->links[$relation])) {
            throw new UnknownLinkException();
        }

        $link = $this->links[$relation];

        return $link;
    }

    public function getLinks()
    {
        return $this->links;
    }

    public function getResourceType()
    {
        return $this->type;
    }

    protected function processItems($items)
    {
        $output = [];
        $typeClass = Types::getTypeClass($this->getResourceType());
        foreach ($items as $item) {
            if (!is_a($item, $typeClass)) {
                throw new InvalidArgumentException('All items must be of type '
                    . $this->getResourceType() . '(' . $typeClass . ')');
            }
            $output[] = $item;
        }

        return $output;
    }

    protected function setCount($count)
    {
        if ($count === null) {
            $this->count = null;

            return;
        }

        if (!is_int($count)) {
            throw new InvalidArgumentException('Count must be int. Got ' . gettype($count));
        }

        if ($count < 0) {
            throw new InvalidArgumentException('Count must be non-negative. Got ' . $count);
        }

        $this->count = $count;
    }

    protected function setLinks($links)
    {
        $this->links = [];

        if (!$links) {
            return;
        }

        foreach ($links as $link) {
            if (!($link instanceof ListLink)) {
                throw new InvalidArgumentException('Link not instanceof ' . ListLink::class);
            }

            $this->links[$link->getRelation()] = $link;
        }
    }

    protected function setType($type)
    {
        if (!Types::has($type)) {
            throw new InvalidArgumentException('Unknown type ' . $type);
        }

        $this->type = $type;
    }
}
