<?php

namespace OneRosterTests\Request;

use OneRoster\Request\IndexOptions;
use OneRosterTests\TestCase;

class IndexOptionsTest extends TestCase
{
    public function testLoadQueryString1()
    {
        $options = $this->fromQS('limit=5');
        $this->assertSame(5, $options->getLimit());
    }

    private function fromQS($query)
    {
        return IndexOptions::fromQueryString($query);
    }
}
