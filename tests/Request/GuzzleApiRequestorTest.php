<?php

namespace OneRosterTests\Request;

use OneRoster\Request\ApiOptions;
use OneRoster\Request\GuzzleApiRequestor;
use OneRoster\Request\Response;
use OneRosterTests\TestCase;
use Psr\Http\Message\MessageInterface as PsrResponse;

class GuzzleApiRequestorTest extends TestCase
{
    public function testBasicRequest()
    {
        $this->requiresSandboxApi();

        $requestor = new GuzzleApiRequestor();
        $options = new ApiOptions();

        $response = $requestor->send('learningdata/v1/orgs', $options);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertInstanceOf(PsrResponse::class, $response->getSourceResponse());
        $this->assertSame(200, $response->getStatusCode());
    }
}
