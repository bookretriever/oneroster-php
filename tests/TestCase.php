<?php

namespace OneRosterTests;

use OneRoster\Request\ApiOptions;
use PHPUnit_Framework_TestCase;

abstract class TestCase extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        parent::setUp();

        ApiOptions::setDefault('bearerToken', 'DEMO_TOKEN');
    }

    protected function getResource($resourceClass, $id = null)
    {
        if ($id === null) {
            $resources = $this->getResourceList($resourceClass);
            $resource = $resources->getFirst();
            if (!$resource) {
                throw new UnexpectedValueException('No resource of class \'' . $resourceClass . '\' was found');
            }

            $id = $resource->getId();
        }

        $resource = call_user_func($resourceClass . '::retrieve', $id);

        return $resource;
    }

    protected function getResourceList($resourceClass)
    {
        return call_user_func($resourceClass . '::index');
    }

    protected function requiresSandboxApi()
    {
        $appId = getenv('CLASSLINK_APP_ID');
        $appToken = getenv('CLASSLINK_APP_TOKEN');

        if (!$appId) {
            $this->markTestSkipped('Missing app ID for sandbox API');
        }

        if (!$appToken) {
            $this->markTestSkipped('Missing app token for sandbox API');
        }

        ApiOptions::setDefault('appId', $appId);
        ApiOptions::setDefault('bearerToken', $appToken);
    }
}
