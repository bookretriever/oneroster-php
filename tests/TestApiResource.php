<?php

namespace OneRosterTests;

use OneRoster\ApiResource;

class TestApiResource extends ApiResource
{
    public function getResourceType()
    {
        return 'test';
    }
}
