<?php

namespace OneRosterTests\Lib;

use OneRoster\ApiObject;
use OneRoster\Linkable;
use Symfony\Component\VarDumper\Caster\Caster;

class VarDumperCaster
{
    public static function castApiObject(ApiObject $obj, $array)
    {
        // TODO: Read actual data value instead of calling toArray
        foreach ($obj->toArray() as $key => $value) {
            $array[Caster::PREFIX_VIRTUAL . $key] = $value;
        }

        return $array;
    }

    public static function castLinkable(Linkable $obj, $array)
    {
        $array['links'] = $obj->getLinks();

        return $array;
    }
}
