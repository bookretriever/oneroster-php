<?php

namespace OneRosterTests\Lib;

use OneRoster\ApiObject;
use OneRoster\Linkable;
use OneRoster\Request\ApiOptions;
use Psy\Configuration;
use Psy\Shell as PsyShell;

class Shell
{
    private $shell;

    public function run()
    {
        $this->setup();

        $this->shell->run();
    }

    public function setup()
    {
        ApiOptions::setDefault('bearerToken', 'DEMO_TOKEN');

        $this->shell = $this->makePsyShell();
    }

    public static function make()
    {
        return new static();
    }

    private function makePsyShell()
    {
        $config = new Configuration();
        $config->getPresenter()->addCasters($this->getCasters());

        $shell = new PsyShell($config);

        return $shell;
    }

    private function getCasters()
    {
        return [
            ApiObject::class => VarDumperCaster::class . '::castApiObject',
            Linkable::class => VarDumperCaster::class . '::castLinkable',
        ];
    }
}
