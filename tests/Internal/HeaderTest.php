<?php

namespace OneRosterTests\Internal;

use OneRoster\Internal\Header;
use OneRosterTests\TestCase;

/**
 * @coversDefaultClass \OneRoster\Internal\Header
 */
class HeaderTest extends TestCase
{
    /**
     * @covers ::parseLink
     * @dataProvider providerTestParseLink
     */
    public function testParseLink($header, $expected)
    {
        $links = Header::parseLink($header);

        $this->assertEquals($expected, $links);
    }

    public function providerTestParseLink()
    {
        return [
            [
                '<https://a.b/c?d=e>; rel="next",<https://f.g/h?i=j>; rel="last"',
                [
                    [
                        '_uri' => 'https://a.b/c?d=e',
                        'rel' => [
                            'next',
                        ],
                    ],
                    [
                        '_uri' => 'https://f.g/h?i=j',
                        'rel' => [
                            'last',
                        ],
                    ],
                ],
            ],
        ];
    }
}
