<?php

namespace OneRosterTests;

use OneRoster\Internal\Str;
use OneRoster\Name;

class NameTest extends TestCase
{
    /**
     * @dataProvider providerTestFormalShort
     */
    public function testFormalShort($format, $expected)
    {
        $value = $this->make($format)->getFormalShort();

        $this->assertSame($expected, $value);
    }

    /**
     * @dataProvider providerTestFullShort
     */
    public function testFullShort($format, $expected)
    {
        $value = $this->make($format)->getFullShort();

        $this->assertSame($expected, $value);
    }

    public function testMake()
    {
        // Who tests the testers? The testers do!
        $name = $this->make('F');
        $this->assertSame('First', $name->first);
        $this->assertSame(null, $name->last);

        $name = $this->make('L');
        $this->assertSame(null, $name->first);
        $this->assertSame('Last', $name->last);
    }

    public function providerTestFormalShort()
    {
        return [
            ['FL', 'Last, First'],
            ['F', 'First'],
            ['L', 'Last'],
        ];
    }

    public function providerTestFullShort()
    {
        return [
            ['FL', 'First Last'],
            ['F', 'First'],
            ['L', 'Last'],
        ];
    }

    private function make($with = null)
    {
        if ($with === null) {
            $with = 'FL';
        }

        $values = [];
        if (Str::contains($with, 'F')) {
            $values['first'] = 'First';
        }

        if (Str::contains($with, 'L')) {
            $values['last'] = 'Last';
        }

        return new Name($values);
    }
}
