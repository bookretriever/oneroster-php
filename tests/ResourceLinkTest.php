<?php

namespace OneRosterTests;

use OneRoster\ResourceLink;
use InvalidArgumentException;

class ResourceLinkTest extends TestCase
{
    /**
     * @var \OneRoster\Link
     */
    private $link;

    public function setUp()
    {
        $this->link = $this->make();
    }

    public function testCreate()
    {
        $this->make();
    }

    /**
     * @dataProvider providerLinkRequiredKeys
     */
    public function testCreateNotString($key)
    {
        $this->setExpectedException(InvalidArgumentException::class);

        $data = $this->getDefaultData();

        $data[$key] = 1;

        $this->make($data);
    }

    /**
     * @dataProvider providerLinkRequiredKeys
     */
    public function testCreateMisssing($key)
    {
        $this->setExpectedException(InvalidArgumentException::class);

        $data = $this->getDefaultData();

        unset($data[$key]);

        $this->make($data);
    }

    public function testGetHref()
    {
        $this->assertSame($this->link->getHref(), 'foo');
    }

    public function testGetRelation()
    {
        $this->assertSame($this->link->getRelation(), 'ham');
    }

    public function testGetSourcedId()
    {
        $this->assertSame($this->link->getSourcedId(), 'bar');
    }

    public function testGetType()
    {
        $this->assertSame($this->link->getResourceType(), 'org');
    }

    public function testToString()
    {
        $this->assertSame((string) $this->link, 'foo');
    }

    public function providerLinkRequiredKeys()
    {
        $keys = [
            'href',
            'sourcedId',
            'type',
        ];

        $argLists = [];
        foreach ($keys as $key) {
            $argLists[] = [$key];
        }

        return $argLists;
    }

    private function getDefaultData(array $override = [])
    {
        $defaultData = [
            'href' => 'foo',
            'sourcedId' => 'bar',
            'type' => 'org',
        ];

        $defaultData = array_merge($defaultData, $override);

        return $defaultData;
    }

    private function make($data = 'default', $relation = null)
    {
        if ($data === 'default') {
            $data = $this->getDefaultData();
        }

        if (!$relation) {
            $relation = 'ham';
        }

        return new ResourceLink($relation, $data);
    }
}
